<?php

namespace App\Http\Controllers;

use App\Actions\Tickets\CreateTicketAction;
use App\Actions\Tickets\DeleteTicketAction;
use App\Actions\Tickets\GetAllTicketsAction;
use App\Actions\Tickets\GetTicketAction;
use App\Actions\Tickets\GetUserTicketsAction;
use App\Actions\Tickets\UpdateTicketAction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

/**
 * @OA\Info(title="Tickets", version="1.0.0")
 */
class TicketController extends Controller
{
    public JsonResponse $result;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @OA\Get(
     *     path="/api/tickets/all",
     *     @OA\Response(response="200", description="ticket list")
     * )
     */
    public function index(): JsonResponse
    {
        return response()->json( app(GetAllTicketsAction::class)->run() );
    }

    /**
     * @OA\Get(
     *     path="/api/tickets/my-tickets",
     *     @OA\Response(response="200", description="user tickets list")
     * )
     */
    public function userTickets(): JsonResponse
    {
        return response()->json( app(GetUserTicketsAction::class)->run(Auth::id()) );
    }

    /**
     * @OA\Get(
     *     path="/api/tickets/ticket/{id}/",
     *     @OA\Response(response="200", description="user ticket detail")
     * )
     */
    public function show(int $id): JsonResponse
    {
        return response()->json( app(GetTicketAction::class)->run($id, Auth::id()) );
    }

    /**
     * @OA\Post(
     *     path="/api/tickets/",
     *     @OA\Response(response="200", description="ticket create")
     * )
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->only('title', 'text');

        return response()->json( app(CreateTicketAction::class)->run($data, Auth::id()) );
    }

    /**
     * @OA\PUT(
     *     path="/api/tickets/{id}",
     *     @OA\Response(response="200", description="ticket update")
     * )
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $data = $request->only('title', 'text');
        return app(UpdateTicketAction::class)->run($data, $id, Auth::id());;
    }

    /**
     * @OA\Delete(
     *     path="/api/tickets/{id}",
     *     @OA\Response(response="200", description="ticket delete")
     * )
     */
    public function delete(int $id): JsonResponse
    {
        return response()->json( app(DeleteTicketAction::class)->run($id) );
    }
}
