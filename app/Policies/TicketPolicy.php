<?php

namespace App\Policies;

use App\Constants\Constants;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TicketPolicy
{
    use HandlesAuthorization;

    /**
     *
     * @param User $user
     * @param Ticket $ticket
     * @return bool
     */
    public function update(User $user, Ticket $ticket): bool
    {
        return (int)$user->id === (int)$ticket->user_id;
    }

    /**
     *
     * @param User $user
     * @return bool
     */
    public function delete(User $user): bool
    {
        return (int)$user->id === Constants::USER_ROLE_ADMIN;
    }
}
