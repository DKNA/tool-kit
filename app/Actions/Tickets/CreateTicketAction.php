<?php

namespace App\Actions\Tickets;

use App\Tasks\Tickets\DeleteTicketTask;

class CreateTicketAction
{
    public function run($data, $user_id)
    {
        return app(DeleteTicketTask::class)->run($data, $user_id);
    }
}
