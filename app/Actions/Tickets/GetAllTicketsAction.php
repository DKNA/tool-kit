<?php

namespace App\Actions\Tickets;

use App\Tasks\Tickets\GetAllTicketsTask;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Инкапсулируют бизнес-логику получения всех заявок
 */
class GetAllTicketsAction
{
    public Collection $result;

    public function __construct()
    {
        $this->result = new Collection();
    }

    /**
     * Исполнительная функция которая собирает в себе необходимые такси для формирования данных
     * (в данном случае только одна таска)
     * Обращаемся к таске, получаем все заявки
     * @return Collection
     * @throws Throwable
     */
    public function run(): Collection
    {
        return app(GetAllTicketsTask::class)->run();
    }
}
