<?php

namespace App\Actions\Tickets;

use App\Tasks\Tickets\GetTicketTask;

class GetUserTicketsAction
{
    public function run($user_id)
    {
        return app(GetTicketTask::class)->run($user_id);
    }
}
