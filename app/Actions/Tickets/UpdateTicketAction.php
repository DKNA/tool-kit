<?php

namespace App\Actions\Tickets;

use App\Tasks\Tickets\UpdateTicketTask;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Gate;

class UpdateTicketAction
{
    public function run($data, $id, $user_id): JsonResponse
    {
        if (!empty($ticket) && Gate::denies('update', $ticket)) {
            $response =  response()->json(['message' => 'Unknown ticket']);
        } else {
            $result = app(UpdateTicketTask::class)->run($data, $id, $user_id);
            if ($result){
                $response = response()->json(['message' => 'Ticket successfully updated']);
            } else {
                $response = response()->json(['message' => 'Unknown ticket']);
            }
        }
        return $response;
    }
}
