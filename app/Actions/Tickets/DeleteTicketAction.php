<?php

namespace App\Actions\Tickets;

use App\Tasks\Tickets\DeleteTicketTask;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Gate;

class DeleteTicketAction
{
    public function run($id): JsonResponse
    {
        if (!empty($ticket) && Gate::denies('delete', $ticket)) {
            $response = response()->json(['message' => 'U don`t have rules for this']);
        } else {
            $result = app(DeleteTicketTask::class)->run($id);
            if ($result){
                $response = response()->json(['message' => 'Ticket successfully deleted']);
            } else {
                $response = response()->json(['message' => 'U don`t have rules for this']);
            }
        }

        return $response;
    }
}
