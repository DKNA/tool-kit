<?php

namespace App\Actions\Tickets;

use App\Tasks\Tickets\GetTicketTask;

class GetTicketAction
{
    public function run($id, $user_id)
    {
        return app(GetTicketTask::class)->run($id, $user_id);
    }
}
