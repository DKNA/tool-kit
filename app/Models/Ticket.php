<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *  definition="Ticket",
 *  @SWG\Property(
 *      property="title",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="text",
 *      type="string"
 *  )
 * @SWG\Property(
 *      property="user_id",
 *      type="ineger"
 *  )
 * )
 */
class Ticket extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'ticket';

    protected $fillable = [
        'title',
        'text',
        'user_id'
    ];

    public static function getCacheTicket()
    {

    }
}
