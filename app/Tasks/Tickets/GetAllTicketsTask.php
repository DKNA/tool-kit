<?php

namespace App\Tasks\Tickets;

use App\Constants\Constants;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Класс GetAllTicketsTask предоставляет методы для получения списка билетов.
 *
 * @package App\Tasks\Tickets
 */
class GetAllTicketsTask
{
    /**
     * Получить все билеты из кэша или из БД, если кэш устарел.
     *
     * @return Collection
     *
     * @SWG\Get(
     *     path="/api/tickets/all",
     *     tags={"Tickets"},
     *     summary="Get all tickets",
     *     description="Get all tickets",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="collection",
     *             @SWG\Items(ref="#/definitions/Ticket")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="Unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/Error"
     *         )
     *     )
     * )
     *
     */
    public function run(): Collection
    {
         return Cache::remember(Constants::CACHE_TICKETS, 3600, function () {
            return Ticket::all();
        });
    }
}
