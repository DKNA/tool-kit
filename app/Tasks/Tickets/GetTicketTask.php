<?php

namespace App\Tasks\Tickets;

use App\Constants\Constants;
use App\Models\Ticket;
use Illuminate\Support\Facades\Cache;

class GetTicketTask
{
    public function run($id, $user_id)
    {
        $ticket = Cache::remember(Constants::CACHE_TICKET_ID . $id, 3600, function () use($id){
            return Ticket::find($id);
        });

        if ($ticket && $ticket->user_id == $user_id) {
            $response = response()->json($ticket);
        } else {
            $response = response()->json(['message' => 'It`s not your ticket']);
        }

        return $response;
    }
}
