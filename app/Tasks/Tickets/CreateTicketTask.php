<?php

namespace App\Tasks\Tickets;

use App\Constants\Constants;
use App\Models\Ticket;
use Illuminate\Support\Facades\Cache;

class CreateTicketTask
{
    public function run($data, $user_id)
    {
        $ticket = Ticket::create([
            'title' => $data['title'],
            'text' => $data['text'],
            'user_id' => $user_id,
        ]);

        if ($ticket) {
            Cache::put(Constants::CACHE_TICKET_ID . $ticket->id, $ticket);
            $this->result = response()->json(['message' => 'Ticket №' . $ticket->id . ' successfully created']);
        } else {
            $this->result = response()->json(['message' => 'Something went wrong']);
        }

        return $this->result;
    }
}
