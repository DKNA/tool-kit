<?php

namespace App\Tasks\Tickets;

use App\Constants\Constants;
use App\Models\Ticket;
use Illuminate\Support\Facades\Cache;

class GetUserTicketsTask
{
    public function run($user_id)
    {
        return Cache::remember(Constants::CACHE_TICKET_USER_ID . $user_id, 3600, function () use ($user_id) {
            return Ticket::where('user_id', $user_id)->get();
        });
    }
}
