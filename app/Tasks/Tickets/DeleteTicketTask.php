<?php

namespace App\Tasks\Tickets;

use App\Constants\Constants;
use App\Models\Ticket;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Gate;

class DeleteTicketTask
{
    public function run($id)
    {
        $ticket = Ticket::find($id);

        $ticket->delete();
        if (Cache::has(Constants::CACHE_TICKET_ID . $ticket->id)) {
            Cache::forget(Constants::CACHE_TICKET_ID . $ticket->id);
        }

        return $ticket;
    }
}
