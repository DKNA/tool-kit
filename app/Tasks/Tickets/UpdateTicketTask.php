<?php

namespace App\Tasks\Tickets;

use App\Constants\Constants;
use App\Models\Ticket;
use Illuminate\Support\Facades\Cache;

class UpdateTicketTask
{
    public function run($data, $id): Ticket
    {
        $ticket = Ticket::find($id);

        $ticket->title = $data['title'];
        $ticket->text = $data['text'];
        $ticket->save();

        Cache::put(Constants::CACHE_TICKET_ID . $ticket->id, $ticket);

        return $ticket;
    }
}
