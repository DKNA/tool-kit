<?php

declare(strict_types=1);

namespace App\Constants;

class Constants
{
    public const USER_ROLE_ADMIN = 1;
    public const CACHE_TICKET_USER_ID = 'tickets:user_id:';
    public const CACHE_TICKET_ID = 'tickets:id:';
    public const CACHE_TICKETS = 'tickets:';
}
