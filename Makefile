.DEFAULT_GOAL := help

build: ## build develoment
	if ! [ -f .env ];then cp .env.example .env;fi
	docker compose up --build -d
	docker compose run --rm app php artisan optimize
	docker compose run --rm app php artisan migrate
	docker compose run --rm app php artisan l5-swagger:generate

optimize: ## artisan optimize
	docker compose run --rm app php artisan optimize

down: ## down develoment
	docker compose down

seed: ## make fake rows
	docker compose run --rm app php artisan db:seed

test: ## make test
	docker compose run --rm app php artisan test

rollback: ## rollback database and down
	docker compose run --rm app php artisan migrate:rollback
	docker compose down

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
