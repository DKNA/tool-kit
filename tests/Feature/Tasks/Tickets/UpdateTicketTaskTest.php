<?php

namespace Tests\Feature\Tasks\Tickets;

use App\Constants\Constants;
use App\Models\Ticket;
use App\Tasks\Tickets\UpdateTicketTask;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class UpdateTicketTaskTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testTicketIsUpdatedSuccessfully()
    {
        $ticket = new Ticket();
        $ticket->user_id = '1';
        $ticket->title = 'title';
        $ticket->text = 'text';
        $ticket->save();

        $updatedTitle = $this->faker->sentence;
        $updatedText = $this->faker->paragraph;

        $data = [
            'title' => $updatedTitle,
            'text' => $updatedText,
        ];

        $updateTicketTask = new UpdateTicketTask();
        $updatedTicket = $updateTicketTask->run($data, $ticket->id);

        // Проверяем, что обновленный тикет соответствует переданным данным
        $this->assertEquals($updatedTitle, $updatedTicket->title);
        $this->assertEquals($updatedText, $updatedTicket->text);

        // Проверяем, что обновленный тикет был сохранен в базу данных
        $this->assertDatabaseHas('ticket', [
            'id' => $ticket->id,
            'title' => $updatedTitle,
            'text' => $updatedText,
            'user_id' => $ticket->user_id
        ]);

        // Проверяем, что обновленный тикет был сохранен в кэш
        $cachedTicket = Cache::get(Constants::CACHE_TICKET_ID . $ticket->id);
        $this->assertEquals($updatedTitle, $cachedTicket->title);
        $this->assertEquals($updatedText, $cachedTicket->text);
    }
}
