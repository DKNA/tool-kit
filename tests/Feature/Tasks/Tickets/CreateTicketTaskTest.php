<?php

namespace Tests\Feature\Tasks\Tickets;

use App\Constants\Constants;
use App\Models\Ticket;
use App\Tasks\Tickets\CreateTicketTask;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

/**
 * @return void
 * @group tickets
 */
class CreateTicketTaskTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function ticket_created_and_cached_succcleaessfully()
    {
        $title = $this->faker->sentence();
        $text = $this->faker->paragraph();
        $user_id = '1';

        $task = new CreateTicketTask();
        $result = $task->run(compact('title', 'text'), $user_id);

        $ticket = Ticket::latest()->first();

        // Убеждаемся, что тикет создан
        $this->assertEquals(1, Ticket::count());
        $this->assertEquals($title, $ticket->title);
        $this->assertEquals($text, $ticket->text);
        $this->assertEquals($user_id, $ticket->user_id);

        // Убеждаемся, что тикет закеширован
        $cachedTicket = Cache::get(Constants::CACHE_TICKET_ID . $ticket->id);

        $ticketArray = json_decode($ticket->toJson(JSON_PRETTY_PRINT), true);
        $cachedTicketArray = json_decode($cachedTicket->toJson(JSON_PRETTY_PRINT), true);
        ksort($ticketArray);
        ksort($cachedTicketArray);

        unset($ticketArray['deleted_at']); // удаляем deleted_at из ожидаемого массива
        $this->assertJson(json_encode($ticketArray), 'Ticket array does not match the cached one.');
        $this->assertJson(json_encode($cachedTicketArray), 'Cached ticket does not match the created one.');



        // Сравниваем кеш и и созданный тикет
        unset($ticket->deleted_at); // удаляем deleted_at из фактического массива
        $this->assertEquals(['message' => 'Ticket №' . $ticket->id . ' successfully created'], json_decode($result->getContent(), true));
        $this->assertEquals(200, $result->status());
    }
}


