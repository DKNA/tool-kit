<?php

namespace Tests\Feature\Tasks\Tickets;

use App\Constants\Constants;
use App\Models\Ticket;
use App\Models\User;
use App\Tasks\Tickets\DeleteTicketTask;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class DeleteTicketTaskTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * Test deleting a ticket.
     *
     * @return void
     */
    public function testDeleteTicket(): void
    {
        // Создаем заявку для удаления
        $ticket = new Ticket();
        $ticket->user_id = '1';
        $ticket->title = 'title';
        $ticket->text = 'text';
        $ticket->save();

        // Аутентифицируемся как администратор
        $admin = User::factory()->create(['role' => '1']);
        $this->actingAs($admin);

        // Пытаемся удалить заявку через API
        $response = $this->json('delete', '/api/tickets' . $ticket->id);
        $response->assertStatus(200);

        // Проверяем, что заявка была удалена из базы данных
        $this->assertNull(Ticket::find($ticket->id));

        // Проверяем, что заявка была удалена из кеша
        $this->assertFalse(Cache::has(Constants::CACHE_TICKET_ID . $ticket->id));
    }

}
