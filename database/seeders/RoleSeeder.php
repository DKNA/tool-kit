<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $role_types = [
            'Клиент',
            'Администратор'
        ];

        foreach($role_types as $key=>$type){
            Role::create([
                'id'    => $key,
                'title' => $type
            ]);
        }
    }
}
