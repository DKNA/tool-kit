<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory(20)->create()
            ->each(function($user)  {
                    Ticket::create([
                        'user_id' => $user->id,
                        'title' => fake()->paragraph(),
                        'text' => fake()->text()
                    ]);
                });

    }
}
